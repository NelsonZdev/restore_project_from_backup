import os
import time
import pipes
import sys
import shutil

# Directory before python project
CONTAINER_PATH = os.path.normpath(os.getcwd() + os.sep + os.pardir)
BACKUP_PATH = CONTAINER_PATH + "/" 

FILES_TO_MODIFY = [
    'INSERT YOUR FILES TO SAVE'
]
FOLDERS_TO_MODIFY = [
    'target',
    'dist'
]

def newPath(name):
    try:
        os.stat(name)
    except FileNotFoundError:
        os.mkdir(name)

def proyectName():
    args = list(sys.argv)
    if (len(args) == 1):
        raise FileNotFoundError("The project name has not been set in args")
    else:
        if (len(args) > 2):
            print("Just need only 1 argument.\nOther args has been ignored")
        return args[1]


def restoreFromOrigin (backupFolder, originFolderBase):
    originFolder = originFolderBase + proyectName() + "/"
        # Files
    for index in FILES_TO_MODIFY:                   # Folders marked
        if (os.path.isfile(backupFolder + "/" + index)):        # Exist files in backup ?   
            if (os.path.isfile(originFolder + index) != True):   # Exist files in origin?   
                shutil.copyfile(backupFolder + "/" + index, originFolder + index)
            else:
                os.remove(originFolder + index)
                shutil.copyfile(backupFolder + "/" + index, originFolder + index) 
    # Folders
    for index in FOLDERS_TO_MODIFY:                             # Folders marked
        if (os.path.isdir(backupFolder + "/" + index)):         # Exist folders in backup?    
            if (os.path.isdir(originFolder + index) != True):   # Exist folders in origin?   
                shutil.copytree(backupFolder + "/" + index, originFolder + index)
            else:
                shutil.rmtree(originFolder + index)
                shutil.copytree(backupFolder + "/" + index, originFolder + index)


def main():
    rollbackPath = BACKUP_PATH + "backup_" + proyectName()
    # BackupFolder
    newPath(rollbackPath)

    # list of folders in current directory
    listOfRollbackPath = os.listdir(rollbackPath)
    if (len(listOfRollbackPath ) > 0):
        restorePath = rollbackPath + "/" + listOfRollbackPath[-1]
        restoreFromOrigin(restorePath, BACKUP_PATH)
    else:
        print ("There are no copies to restore ")


if __name__ == "__main__":
    main()
