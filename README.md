# restore_project_from_backup

Simple script for restore your documents, after being modified by a CI program.
Restore last copy in your backup folder

----
## Recommended to use with [rollback_folders](https://gitlab.com/NelsonZdev/rollback_folders)
----

## You can add files and folders to restore
    - Mofifies:
        FILES_TO_MODIFY['your_file', 'your_file_2']
        FOLDERS_TO_MODIFY['your_folder', 'your_folder_2']


## To change the origin and target directory :
    CONTAINER_PATH = os.path.normpath(os.getcwd() + os.sep + os.pardir) for
    CONTAINER_PATH = "Your path" -Ejem "/home/[jourName]/target" or "C://target" (**not tested with windows**)
### In case of not using [rollback_folders](https://gitlab.com/NelsonZdev/rollback_folders)
    restoreFromOrigin([your_backup_folder] + [files_and_folders_to_restore], [your_proyect_to_restore_path] + [files_and_folders_to_restore])

## Thought structure:
```
root/
│
└───your_jenkins_home_folder/
    │
    └───your_project/
    │   │   **this_program**/
    │   │   your_proyect_folders/
    │   │   your_proyect_files
    │   │   ...
    │
    └───rollbacks_out/ (backup_folders)
        │   your_proyect_folders_back/
        │   your_proyect_files_back
        │   ...
```
## In linux you can run the program: 
    - With the command **./run.sh [folder_name]** or **sh run.sh [folder_name]** into a your proyect folder

## On other operating systems run:
    - this_project_folder/**python src/app.py [folder_name]**
### or
    - **python [full path]/your_project/this_project/src/app.py [folder_name]** 

### Thought for jenkins CI.

----

## Use it as you wish - Nelson Zapata :)
